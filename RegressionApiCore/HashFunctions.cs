﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace RegressionApiCore
{
    public class HashFunctions
    {
        public static byte[] saltGenerate()
        {
            byte[] salt = new byte[128 / 8];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(salt);
            }
            return salt;
        }

        public static string hashPassword(string originalPassword, byte[] salt)
        {
            string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(password: originalPassword, salt: salt, prf: KeyDerivationPrf.HMACSHA1, iterationCount: 1000, numBytesRequested: 256 / 8));
            return hashed;
        }
    }
}
