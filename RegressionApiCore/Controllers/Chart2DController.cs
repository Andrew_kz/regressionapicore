﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RegressionApiCore.Models;
using RegressionApiCore.Calculation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RegressionApiCore.Controllers
{
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class Chart2DController : Controller
    {
        [HttpPost]
        [Route("api/linear2dchart")]
        public IActionResult Linear2DChart(BaseChartData2D data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ChartData2D chart = Calculate2DChartData.Linear2DChart(data.baseXData, data.regressionCoef, data.x1Index, data.property);
            return Json(chart);
        }

        [HttpPost]
        [Route("api/degree2dchart")]
        public IActionResult Degree2DChart(BaseChartData2D data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ChartData2D chart = Calculate2DChartData.Degree2DChart(data.baseXData, data.regressionCoef, data.x1Index, data.property);
            return Json(chart);
        }

        [HttpPost]
        [Route("api/exponential2dchart")]
        public IActionResult Exponential2DChart(BaseChartData2D data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ChartData2D chart = Calculate2DChartData.Exponential2DChart(data.baseXData, data.regressionCoef, data.x1Index, data.property);
            return Json(chart);
        }

        [HttpPost]
        [Route("api/hyperbola2dchart")]
        public IActionResult Hyperbola2DChart(BaseChartData2D data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ChartData2D chart = Calculate2DChartData.Hyperbola2DChart(data.baseXData, data.regressionCoef, data.x1Index, data.property);
            return Json(chart);
        }

        [HttpPost]
        [Route("api/square2dchart")]
        public IActionResult SquareDChart(BaseChartData2D data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ChartData2D chart = Calculate2DChartData.Square2DChart(data.baseXData, data.regressionCoef, data.x1Index, data.property);
            return Json(chart);
        }

        [HttpPost]
        [Route("api/polynomial2dchart")]
        public IActionResult PolynomialDChart(BasePolynimialChartData2D data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ChartData2D chart = Calculate2DChartData.Polynomial2DChart(data.baseXData, data.regressionCoef, data.x1Index, data.property, data.polynomialDegree);
            return Json(chart);
        }
    }
}
