﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RegressionApiCore.Models;
using RegressionApiCore.Calculation;

namespace RegressionApiCore.Controllers
{
    //[Route("api/[controller]")]
    [ApiController]
    public class Chart3DController : Controller
    {
        [HttpPost]
        [Route("api/linear3dchart")]
        public IActionResult Linear3DChart(BaseChartData3D data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ChartData3D chart = Calculate3DChartData.Linear3DChart(data.baseXData, data.regressionCoef, data.x1Index, data.x2Index, data.property);
            return Json(chart);
        }

        [HttpPost]
        [Route("api/exponential3dchart")]
        public IActionResult Exponential3DChart(BaseChartData3D data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ChartData3D chart = Calculate3DChartData.Exponential3DChart(data.baseXData, data.regressionCoef, data.x1Index, data.x2Index, data.property);
            return Json(chart);
        }

        [HttpPost]
        [Route("api/hyperbola3dchart")]
        public IActionResult Hyperbola3DChart(BaseChartData3D data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ChartData3D chart = Calculate3DChartData.Hyperbola3DChart(data.baseXData, data.regressionCoef, data.x1Index, data.x2Index, data.property);
            return Json(chart);
        }

        [HttpPost]
        [Route("api/square3dchart")]
        public IActionResult Square3DChart(BaseChartData3D data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ChartData3D chart = Calculate3DChartData.Square3DChart(data.baseXData, data.regressionCoef, data.x1Index, data.x2Index, data.property);
            return Json(chart);
        }

        [HttpPost]
        [Route("api/degree3dchart")]
        public IActionResult Degree3DChart(BaseChartData3D data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ChartData3D chart = Calculate3DChartData.Degree3DChart(data.baseXData, data.regressionCoef, data.x1Index, data.x2Index, data.property);
            return Json(chart);
        }

        [HttpPost]
        [Route("api/polynomial3dchart")]
        public IActionResult Polynomial3DChart(BasePolynimialChartData3D data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ChartData3D chart = Calculate3DChartData.Polynomial3DChart(data.baseXData, data.regressionCoef, data.x1Index, data.x2Index, data.property, data.polynomialDegree);
            return Json(chart);
        }
    }
}