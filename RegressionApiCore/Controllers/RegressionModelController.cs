﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RegressionApiCore.Models;
using RegressionApiCore.Calculation;

namespace RegressionApiCore.Controllers
{
    //[Route("api/[controller]")]
    [ApiController]
    public class RegressionModelController : Controller
    {
        [HttpPost]
        [Route("api/linearmodel")]
        public IActionResult LinearModel(BaseCalculationData data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            RegressionData model = RegressionModel.linearRegression(data.baseXData, data.baseYData);
            return Json(model);

        }

        [HttpPost]
        [Route("api/degreemodel")]
        public IActionResult DegreeModel(BaseCalculationData data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            RegressionData model = RegressionModel.degreeResression(data.baseXData, data.baseYData);
            return Json(model);

        }

        [HttpPost]
        [Route("api/hyperbolamodel")]
        public IActionResult HyperbolaModel(BaseCalculationData data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            RegressionData model = RegressionModel.hyperbolaResression(data.baseXData, data.baseYData);
            return Json(model);

        }

        [HttpPost]
        [Route("api/exponentialmodel")]
        public IActionResult ExponentialModel(BaseCalculationData data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            RegressionData model = RegressionModel.exponentialRegression(data.baseXData, data.baseYData);
            return Json(model);

        }

        [HttpPost]
        [Route("api/squaremodel")]
        public IActionResult SquareModel(BaseCalculationData data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            RegressionData model = RegressionModel.squareRegression(data.baseXData, data.baseYData);
            return Json(model);

        }

        [HttpPost]
        [Route("api/polynomialmodel")]
        public IActionResult PolynomialModel(BasePolynomialCalculationData data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            RegressionData model = RegressionModel.polymonialRegression(data.baseXData, data.baseYData, data.polynomialDegree);
            return Json(model);

        }
    }
}