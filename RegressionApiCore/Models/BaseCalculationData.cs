﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace RegressionApiCore.Models
{
    public class BaseCalculationData
    {
        [Required(ErrorMessage = "Wrong X data")]
        public double[,] baseXData { get; set; }

        [Required(ErrorMessage = "Wrong Y data")]
        public double[] baseYData { get; set; }

    }

    public class BasePolynomialCalculationData
    {
        [Required(ErrorMessage = "Wrong X data")]
        public double[,] baseXData { get; set; }

        [Required(ErrorMessage = "Wrong Y data")]
        public double[] baseYData { get; set; }

        [Required(ErrorMessage = "Wrong polynomial degree")]
        public int polynomialDegree { get; set; }
    }
}
