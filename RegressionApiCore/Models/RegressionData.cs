﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RegressionApiCore.Models
{
    public class RegressionData
    {
        public double[] calculatedY { get; set; }
        public double[] regressionCoef { get; set; }
        public double R { get; set; }
        public double korrelation { get; set; }
        public string regression_equation { get; set; }

        public RegressionData(double[] X, double[] Y)
        {
            calculatedY = new double[Y.Length];
            regressionCoef = new double[X.Length + 1];
        }
        public RegressionData(double[,] X, double[] Y)
        {
            calculatedY = new double[Y.Length];
            regressionCoef = new double[X.GetLength(1)];
        }
    }
}
