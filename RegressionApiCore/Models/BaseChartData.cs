﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace RegressionApiCore.Models
{
    public class BaseChartData2D
    {

        [Required(ErrorMessage = "Wrong X data")]
        public double[,] baseXData { get; set; }

        [Required(ErrorMessage = "Wrong regression coefficient")]
        public double[] regressionCoef { get; set; }

        [Required(ErrorMessage = "Wrong x index")]
        public int x1Index { get; set; }

        [Required(ErrorMessage = "Wrong x property")]
        public string property { get; set; }
    }

    public class BasePolynimialChartData2D
    {

        [Required(ErrorMessage = "Wrong X data")]
        public double[,] baseXData { get; set; }

        [Required(ErrorMessage = "Wrong regression coefficient")]
        public double[] regressionCoef { get; set; }

        [Required(ErrorMessage = "Wrong x index")]
        public int x1Index { get; set; }

        [Required(ErrorMessage = "Wrong x property")]
        public string property { get; set; }

        [Required(ErrorMessage = "Wrong polynomial degree")]
        public int polynomialDegree { get; set; }
    }

    public class BaseChartData3D
    {

        [Required(ErrorMessage = "Wrong X data")]
        public double[,] baseXData { get; set; }

        [Required(ErrorMessage = "Wrong regression coefficient")]
        public double[] regressionCoef { get; set; }

        [Required(ErrorMessage = "Wrong x index")]
        public int x1Index { get; set; }

        [Required(ErrorMessage = "Wrong x index")]
        public int x2Index { get; set; }

        [Required(ErrorMessage = "Wrong x property")]
        public string property { get; set; }
    }

    public class BasePolynimialChartData3D
    {

        [Required(ErrorMessage = "Wrong X data")]
        public double[,] baseXData { get; set; }

        [Required(ErrorMessage = "Wrong regression coefficient")]
        public double[] regressionCoef { get; set; }

        [Required(ErrorMessage = "Wrong x index")]
        public int x1Index { get; set; }

        [Required(ErrorMessage = "Wrong x index")]
        public int x2Index { get; set; }

        [Required(ErrorMessage = "Wrong x property")]
        public string property { get; set; }

        [Required(ErrorMessage = "Wrong polynomial degree")]
        public int polynomialDegree { get; set; }
    }
}
