﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RegressionApiCore.Models
{
    public class ChartData2D
    {
        public int x1index { get; set; }

        public double[] Y { get; set; }
        public double[] x1Data { get; set; }

        public ChartData2D(int index1, int yLength, int x1Length)
        {
            x1index = index1;
            Y = new double[yLength];
            x1Data = new double[x1Length];
        }
    }

    public class ChartData3D : ChartData2D
    {
        public int x2index { get; set; }
        public double[] x2Data { get; set; }

        public ChartData3D(int index1, int index2, int yLength, int x1Length, int x2Length) : base(index1, yLength, x1Length)
        {
            x2index = index2;
            x2Data = new double[x2Length];
        }
    }
}
