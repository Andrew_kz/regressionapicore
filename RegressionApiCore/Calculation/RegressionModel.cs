﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using RegressionApiCore.Models;

namespace RegressionApiCore.Calculation
{
    public class RegressionModel
    {
        public static double[] baseRegression(double[,] X, double[] Y)
        {


            double[] YRegressionData = new double[Y.Length];
            Matrix<double> Xvalue = DenseMatrix.OfArray(X);
            Vector<double> Yvalue = DenseVector.OfArray(Y);
            Matrix<double> XvalueTransparent;
            Matrix<double> result1;
            Vector<double> result2;
            Vector<double> result;
            XvalueTransparent = Xvalue.Transpose();
            result1 = Xvalue * XvalueTransparent;
            result2 = Xvalue * Yvalue;
            result = result1.Inverse() * result2;
            return result.ToArray();
        }

        private static double[,] addRegressionX(double[,] X)
        {
            double[,] regressionX = new double[X.GetLength(0) + 1, X.GetLength(1)];
            for (int i = 0; i < X.GetLength(1); i++)
            {
                regressionX[0, i] = 1;
            }
            for (int i = 0; i < X.GetLength(0); i++)
            {
                for (int j = 0; j < X.GetLength(1); j++)
                {
                    regressionX[i + 1, j] = X[i, j];
                }
            }
            return regressionX;

        }

        public static RegressionData linearRegression(double[,] X, double[] Y)
        {
            double[,] regressionX = addRegressionX(X);
            RegressionData model = new RegressionData(regressionX, Y);
            model.regressionCoef = baseRegression(regressionX, Y);

            for (int i = 0; i < Y.Length; i++)
            {
                model.calculatedY[i] = model.regressionCoef[0];
                for (int j = 1; j < model.regressionCoef.Length; j++)
                {
                    model.calculatedY[i] += regressionX[j, i] * model.regressionCoef[j];
                }
            }

            model.regression_equation = ResultEquation.linearRegressionResult(model.regressionCoef);
            model.korrelation = Aproxymation.korrelation(model.calculatedY, Y);

            return model;
        }

        public static RegressionData degreeResression(double[,] X, double[] Y)
        {
            X = addRegressionX(X);
            RegressionData model = new RegressionData(X, Y);

            double[,] XData = new double[X.GetLength(0), Y.Length];
            double[] YData = new double[Y.Length];
            for (int j = 0; j < Y.Length; j++)
            {
                YData[j] = Math.Log10(Y[j]);
                XData[0, j] = X[0, j];
                for (int i = 1; i < X.Length / Y.Length; i++)
                {
                    XData[i, j] = Math.Log10(X[i, j]);
                    if (XData[i, j] == double.NaN && double.IsInfinity(XData[i, j]))
                    {
                        XData[i, j] = 0;
                    }
                }
                if (YData[j] == double.NaN && double.IsInfinity(YData[j]))
                {
                    YData[j] = 0;
                }

            }

            model.regressionCoef = baseRegression(XData, YData);

            for (int i = 0; i < Y.Length; i++)
            {
                model.calculatedY[i] = Math.Pow(10, model.regressionCoef[0]);
                for (int j = 1; j < model.regressionCoef.Length; j++)
                {
                    model.calculatedY[i] *= Math.Pow(X[j, i], model.regressionCoef[j]);
                }
            }
            model.regressionCoef[0] = Math.Pow(10, model.regressionCoef[0]);
            model.regression_equation = ResultEquation.degreeRegressionResult(model.regressionCoef);
            model.korrelation = Aproxymation.korrelationIndex(model.calculatedY, Y);
            return model;
        }

        public static RegressionData hyperbolaResression(double[,] X, double[] Y)
        {
            X = addRegressionX(X);
            RegressionData model = new RegressionData(X, Y);

            double[,] XData = new double[X.GetLength(0), Y.Length];
            double[] YData = new double[Y.Length];
            for (int j = 0; j < Y.Length; j++)
            {
                XData[0, j] = X[0, j];
                YData[j] = Y[j];
                for (int i = 1; i < X.Length / Y.Length; i++)
                {
                    XData[i, j] = 1 / X[i, j];
                    if (XData[i, j] == double.NaN && double.IsInfinity(XData[i, j]))
                    {
                        //data.errorcount++;
                        XData[i, j] = 0;
                    }
                }
            }

            model.regressionCoef = baseRegression(XData, YData);

            for (int i = 0; i < Y.Length; i++)
            {
                model.calculatedY[i] = model.regressionCoef[0];
                for (int j = 1; j < model.regressionCoef.Length; j++)
                {
                    model.calculatedY[i] += model.regressionCoef[j] * (XData[j, i]);
                }
            }
            model.regression_equation = ResultEquation.hyperbolaRegressionResult(model.regressionCoef);
            model.korrelation = Aproxymation.korrelationIndex(model.calculatedY, Y);
            return model;
        }

        public static RegressionData exponentialRegression(double[,] X, double[] Y)
        {
            X = addRegressionX(X);
            RegressionData model = new RegressionData(X, Y);
            double[,] XData = new double[X.GetLength(0), Y.Length];
            double[] YData = new double[Y.Length];
            //data.errorcount = 0;
            for (int j = 0; j < Y.Length; j++)
            {
                YData[j] = Math.Log(Y[j]);
                if (YData[j] == double.NaN && double.IsInfinity(YData[j]))
                {
                    //data.errorcount++;
                    YData[j] = 0;
                }
            }

            model.regressionCoef = baseRegression(X, YData);

            for (int i = 0; i < Y.Length; i++)
            {
                double sum = 0;
                for (int j = 1; j < model.regressionCoef.Length; j++)
                {
                    sum += X[j, i] * model.regressionCoef[j];
                }
                model.calculatedY[i] = Math.Exp(model.regressionCoef[0]) * Math.Exp(sum);
            }
            model.regressionCoef[0] = Math.Exp(model.regressionCoef[0]);
            model.regression_equation = ResultEquation.exponentialRegressionResult(model.regressionCoef);
            model.korrelation = Aproxymation.korrelationIndex(model.calculatedY, Y);
            return model;
        }

        public static RegressionData squareRegression(double[,] X, double[] Y)
        {
            X = addRegressionX(X);
            RegressionData model = new RegressionData(X, Y);
            double[,] XData = new double[X.GetLength(0), Y.Length];
            double[] YData = new double[Y.Length];
            for (int j = 0; j < Y.Length; j++)
            {
                XData[0, j] = X[0, j];
                YData[j] = Y[j];
                for (int i = 1; i < X.Length / Y.Length; i++)
                {
                    XData[i, j] = Math.Sqrt(X[i, j]);
                    if (XData[i, j] == double.NaN && double.IsInfinity(XData[i, j]))
                    {
                        //data.errorcount++;
                        XData[i, j] = 0;
                    }
                }
            }

            model.regressionCoef = baseRegression(XData, YData);

            for (int i = 0; i < Y.Length; i++)
            {
                model.calculatedY[i] = model.regressionCoef[0];
                for (int j = 1; j < model.regressionCoef.Length; j++)
                {
                    model.calculatedY[i] += model.regressionCoef[j] * (XData[j, i]);
                }
            }
            model.regression_equation = ResultEquation.squareRegressionResult(model.regressionCoef);
            model.korrelation = Aproxymation.korrelationIndex(model.calculatedY, Y);
            return model;

        }

        public static RegressionData polymonialRegression(double[,] X, double[] Y, int n)
        {
            X = addRegressionX(X);
            RegressionData model = new RegressionData(X, Y);
            double[,] XData = new double[(X.GetLength(0) - 1) * n + 1, Y.Length];
            double[] YData = new double[Y.Length];
            int k = 1;
            for (int j = 0; j < Y.Length; j++)
            {
                XData[0, j] = X[0, j];
                YData[j] = Y[j];
            }
            for (int i = 1; i < ((X.GetLength(0)) - 1) * n + 1; i += n)
            {
                for (int j = 0; j < Y.Length; j++)
                {
                    for (int l = 0; l < n; l++)
                    {
                        XData[i + l, j] = Math.Pow(X[k, j], l + 1);
                    }
                }
                k++;
            }

            model.regressionCoef = baseRegression(XData, YData);
            for (int i = 0; i < Y.Length; i++)
            {
                model.calculatedY[i] = model.regressionCoef[0];

            }
            k = 1;
            for (int i = 1; i < ((X.GetLength(0)) - 1) * n + 1; i += n)
            {
                for (int j = 0; j < Y.Length; j++)
                {
                    for (int l = 0; l < n; l++)
                    {
                        model.calculatedY[j] += model.regressionCoef[i + l] * Math.Pow(X[k, j], l + 1);
                    }
                }
                k++;
            }

            model.regression_equation = ResultEquation.polynomialRegressionResult(model.regressionCoef, n);
            model.korrelation = Aproxymation.korrelationIndex(model.calculatedY, Y);
            return model;
        }
    }
}
