﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace RegressionApiCore.Calculation
{
    public class Parsing
    {
        public static double[] ParsingY(string baseY)
        {
            var reg = Regex.Matches(baseY, @"([+\-])?[0-9]+(\,[0-9]+)?");

            double[] y = new double[reg.Count];

            for (int i = 0; i < reg.Count; i++)
            {
                y[i] = Double.Parse(reg[i].Value);
            }
            return y;
        }

        public static double[,] ParsingX(string baseX)
        {
            string[] lines = baseX.Split(new Char[] { ';' });
            double[,] x = new double[lines.Length, Regex.Matches(lines[0], @"[0-9]+(\,[0-9]+)?").Count];
            for (int i = 0; i < lines.Length; i++)
            {
                var reg = Regex.Matches(lines[i], @"[0-9]+(\,[0-9]+)?");
                for (int j = 0; j < reg.Count; j++)
                {
                    x[i, j] = Double.Parse(reg[j].Value);
                }
            }

            return x;
        }
    }
}
