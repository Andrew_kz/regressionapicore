﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RegressionApiCore.Calculation
{
    public class Aproxymation
    {
        public static double korrelation(double[] calculatedY, double[] Y)
        {
            double baseDeviation = 0;
            double regressionDeviation = 0;
            double Yaverage = Y.Average();
            double regressionYaverage = calculatedY.Average();
            double korrelation = 0;
            for (int i = 0; i < calculatedY.Length; i++)
            {
                baseDeviation += Math.Pow((Y[i] - Yaverage), 2);
                regressionDeviation += Math.Pow((calculatedY[i] - Yaverage), 2);
            }
            korrelation = Math.Sqrt(regressionDeviation / baseDeviation);

            if (double.IsNaN(korrelation))
            {
                korrelation = 0;
            }
            return korrelation;
        }

        public static double R(double korrelation)
        {
            return Math.Pow(korrelation, 2);
        }

        public static double korrelationIndex(double[] calculatedY, double[] Y)
        {
            double baseDeviation = 0;
            double regressionDeviation = 0;
            double Yaverage = Y.Average();
            double regressionYaverage = calculatedY.Average();
            double korrelationIndex = 0;
            for (int i = 0; i < calculatedY.Length; i++)
            {
                baseDeviation += Math.Pow((calculatedY[i] - Y[i]), 2);
                regressionDeviation += Math.Pow((Y[i] - Yaverage), 2);
            }
            korrelationIndex = Math.Sqrt(1 - (baseDeviation / regressionDeviation));

            if (double.IsNaN(korrelationIndex))
            {
                korrelationIndex = 0;
            }
            return korrelationIndex;

        }

        public static double Radj(double R, int yLength, int xLength)
        {
            double rAjd = 1 - (1 - R) * (yLength - 1) / (yLength - (xLength - 1) - 1);
            return rAjd;
        }
    }
}
