﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RegressionApiCore.Models;


namespace RegressionApiCore.Calculation
{
    public class Calculate2DChartData
    {
        public static ChartData2D Linear2DChart(double[,] X, double[] regressionCoef, int index1, string property)
        {
            ChartData2D data = new ChartData2D(index1, 100, 100);
            double[] X1 = new double[X.GetLength(1)];
            double[] XtempAll = new double[X.GetLength(1)];
            double sumOther = 0;
            for (int i = 0; i < X.GetLength(1); i++)
            {
                X1[i] = X[index1 - 1, i];
            }

            for (int i = 0; i < X.GetLength(0); i++)
            {
                if (i != index1 - 1)
                {
                    for (int l = 0; l < X.GetLength(1); l++)
                    {
                        XtempAll[l] = X[i, l];
                    }
                    switch (property)
                    {
                        case "min":
                            sumOther += regressionCoef[i + 1] * XtempAll.Min();
                            break;
                        case "avg":
                            sumOther += regressionCoef[i + 1] * XtempAll.Average();
                            break;
                        case "max":
                            sumOther += regressionCoef[i + 1] * XtempAll.Max();
                            break;
                    }
                }
            }

            double localXmin = X1.Min();
            for (int i = 0; i < 100; i++)
            {

                data.Y[i] = regressionCoef[0] + regressionCoef[index1] * localXmin + sumOther;
                data.x1Data[i] = localXmin;
                localXmin += (X1.Max() - X1.Min()) / 100;
            }

            return data;
        }

        public static ChartData2D Exponential2DChart(double[,] X, double[] regressionCoef, int index1, string property)
        {
            ChartData2D data = new ChartData2D(index1, 100, 100);
            double[] X1 = new double[X.GetLength(1)];
            double[] XtempAll = new double[X.GetLength(1)];
            double sumOther = 0;
            for (int i = 0; i < X.GetLength(1); i++)
            {
                X1[i] = X[index1 - 1, i];
            }

            for (int i = 0; i < X.GetLength(0); i++)
            {
                if (i != index1 - 1)
                {
                    for (int l = 0; l < X.GetLength(1); l++)
                    {
                        XtempAll[l] = X[i, l];
                    }
                    switch (property)
                    {
                        case "min":
                            sumOther += regressionCoef[i + 1] * XtempAll.Min();
                            break;
                        case "avg":
                            sumOther += regressionCoef[i + 1] * XtempAll.Average();
                            break;
                        case "max":
                            sumOther += regressionCoef[i + 1] * XtempAll.Max();
                            break;
                    }
                }
            }

            double localX = X1.Min();
            for (int i = 0; i < 100; i++)
            {

                data.Y[i] = regressionCoef[0] * Math.Exp(regressionCoef[index1] * localX + sumOther);
                data.x1Data[i] = localX;
                localX += (X1.Max() - X1.Min()) / 100;
            }

            return data;
        }

        public static ChartData2D Polynomial2DChart(double[,] X, double[] regressionCoef, int index1, string property, int n)
        {
            ChartData2D data = new ChartData2D(index1, 100, 100);
            double[] X1 = new double[X.GetLength(1)];
            double[] XtempAll = new double[X.GetLength(1)];
            double sumOther = 0;
            for (int i = 0; i < X.GetLength(1); i++)
            {
                X1[i] = X[index1 - 1, i];
            }

            for (int i = 0; i < X.GetLength(0); i++)
            {
                if (i != index1 - 1)
                {
                    for (int h = 0; h < X.GetLength(1); h++)
                    {
                        XtempAll[h] = X[i, h];
                    }
                    int l = 1;
                    for (int s = i * n + 1; s < i * n + n + 1; s++)
                    {
                        switch (property)
                        {
                            case "min":
                                sumOther += regressionCoef[s] * Math.Pow(XtempAll.Min(), l);
                                break;
                            case "avg":
                                sumOther += regressionCoef[s] * Math.Pow(XtempAll.Average(), l);
                                break;
                            case "max":
                                sumOther += regressionCoef[s] * Math.Pow(XtempAll.Max(), l);
                                break;
                        }
                        l++;
                    }
                }
            }

            double localX = X1.Min();
            for (int i = 0; i < 100; i++)
            {
                double sum1 = 0;
                int l = 1;
                for (int m = (index1 - 1) * n + 1; m < (index1 - 1) * n + n + 1; m++)
                {
                    sum1 += regressionCoef[m] * Math.Pow(localX, l);
                    l++;
                }
                data.x1Data[i] = localX;
                localX += (X1.Max() - X1.Min()) / 100;
                data.Y[i] = regressionCoef[0] + sum1 + sumOther;
            }
            return data;
        }

        public static ChartData2D Degree2DChart(double[,] X, double[] regressionCoef, int index1, string property)
        {
            ChartData2D data = new ChartData2D(index1, 100, 100);
            double[] X1 = new double[X.GetLength(1)];
            double[] XtempAll = new double[X.GetLength(1)];
            double sumOther = 1;
            for (int i = 0; i < X.GetLength(1); i++)
            {
                X1[i] = X[index1 - 1, i];
            }

            for (int i = 0; i < X.GetLength(0); i++)
            {
                if (i != index1 - 1)
                {
                    for (int l = 0; l < X.GetLength(1); l++)
                    {
                        XtempAll[l] = X[i, l];
                    }
                    switch (property)
                    {
                        case "min":
                            sumOther *= Math.Pow(XtempAll.Min(), regressionCoef[i + 1]);
                            break;
                        case "avg":
                            sumOther *= Math.Pow(XtempAll.Average(), regressionCoef[i + 1]);
                            break;
                        case "max":
                            sumOther *= Math.Pow(XtempAll.Max(), regressionCoef[i + 1]);
                            break;
                    }
                }
            }

            double localX = X1.Min();
            for (int i = 0; i < 100; i++)
            {

                data.Y[i] = regressionCoef[0] * Math.Pow(localX, regressionCoef[index1]) * sumOther;
                data.x1Data[i] = localX;
                localX += (X1.Max() - X1.Min()) / 100;
            }

            return data;
        }

        public static ChartData2D Hyperbola2DChart(double[,] X, double[] regressionCoef, int index1, string property)
        {
            ChartData2D data = new ChartData2D(index1, 100, 100);
            double[] X1 = new double[X.GetLength(1)];
            double[] XtempAll = new double[X.GetLength(1)];
            double sumOther = 0;
            for (int i = 0; i < X.GetLength(1); i++)
            {
                X1[i] = X[index1 - 1, i];
            }

            for (int i = 0; i < X.GetLength(0); i++)
            {
                if (i != index1 - 1)
                {
                    for (int l = 0; l < X.GetLength(1); l++)
                    {
                        XtempAll[l] = X[i, l];
                    }
                    switch (property)
                    {
                        case "min":
                            sumOther += regressionCoef[i + 1] / XtempAll.Min();
                            break;
                        case "avg":
                            sumOther += regressionCoef[i + 1] / XtempAll.Average();
                            break;
                        case "max":
                            sumOther += regressionCoef[i + 1] / XtempAll.Max();
                            break;
                    }
                }
            }

            double localX = X1.Min();
            for (int i = 0; i < 100; i++)
            {

                data.Y[i] = regressionCoef[0] + regressionCoef[index1] / localX + sumOther;
                data.x1Data[i] = localX;
                localX += (X1.Max() - X1.Min()) / 100;
            }

            return data;
        }

        public static ChartData2D Square2DChart(double[,] X, double[] regressionCoef, int index1, string property)
        {
            ChartData2D data = new ChartData2D(index1, 100, 100);
            double[] X1 = new double[X.GetLength(1)];
            double[] XtempAll = new double[X.GetLength(1)];
            double sumOther = 0;
            for (int i = 0; i < X.GetLength(1); i++)
            {
                X1[i] = X[index1 - 1, i];
            }

            for (int i = 0; i < X.GetLength(0); i++)
            {
                if (i != index1 - 1)
                {
                    for (int l = 0; l < X.GetLength(1); l++)
                    {
                        XtempAll[l] = X[i, l];
                    }
                    switch (property)
                    {
                        case "min":
                            sumOther += regressionCoef[i + 1] * Math.Sqrt(XtempAll.Min());
                            break;
                        case "avg":
                            sumOther += regressionCoef[i + 1] * Math.Sqrt(XtempAll.Average());
                            break;
                        case "max":
                            sumOther += regressionCoef[i + 1] * Math.Sqrt(XtempAll.Max());
                            break;
                    }
                }
            }

            double localX = X1.Min();
            for (int i = 0; i < 100; i++)
            {

                data.Y[i] = regressionCoef[0] + regressionCoef[index1] * Math.Sqrt(localX) + sumOther;
                data.x1Data[i] = localX;
                localX += (X1.Max() - X1.Min()) / 100;
            }

            return data;
        }
    }
}
