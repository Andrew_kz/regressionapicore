﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RegressionApiCore.Models;

namespace RegressionApiCore.Calculation
{
    public class Calculate3DChartData
    {

            public static ChartData3D Linear3DChart(double[,] X, double[] regressionCoef, int index1, int index2, string property)
            {
                ChartData3D data = new ChartData3D(index1, index2, 2500, 50, 50);
                double[] X1 = new double[X.GetLength(1)];
                double[] X2 = new double[X.GetLength(1)];
                double[] XtempAll = new double[X.GetLength(1)];
                double sumOther = 0;
                for (int i = 0; i < X.GetLength(1); i++)
                {
                    X1[i] = X[index1 - 1, i];
                    X2[i] = X[index2 - 1, i];
                }

                for (int i = 0; i < X.GetLength(0); i++)
                {
                    if ((i != index1 - 1) && (i != index2 - 1))
                    {
                        for (int l = 0; l < X.GetLength(1); l++)
                        {
                            XtempAll[l] = X[i, l];
                        }
                        switch (property)
                        {
                            case "min":
                                sumOther += regressionCoef[i + 1] * XtempAll.Min();
                                break;
                            case "avg":
                                sumOther += regressionCoef[i + 1] * XtempAll.Average();
                                break;
                            case "max":
                                sumOther += regressionCoef[i + 1] * XtempAll.Max();
                                break;
                        }
                    }
                }

                double localX1 = X1.Min();
                double localX2 = X2.Min();
                for (int i = 0; i < data.x1Data.Length; i++)
                {
                    data.x1Data[i] = localX1;
                    data.x2Data[i] = localX2;
                    localX1 += (X1.Max() - X1.Min()) / 50;
                    localX2 += (X2.Max() - X2.Min()) / 50;
                }
                for (int i = 0; i < 50; i++)
                {
                    for (int j = 0; j < 50; j++)
                    {
                        data.Y[i * data.x1Data.Length + j] = regressionCoef[0] + regressionCoef[index1] * data.x1Data[j] + regressionCoef[index2] * data.x2Data[i] + sumOther;
                    }
                }

                return data;
            }

            public static ChartData3D Exponential3DChart(double[,] X, double[] regressionCoef, int index1, int index2, string property)
            {
                ChartData3D data = new ChartData3D(index1, index2, 2500, 50, 50);
                double[] X1 = new double[X.GetLength(1)];
                double[] X2 = new double[X.GetLength(1)];
                double[] XtempAll = new double[X.GetLength(1)];
                double sumOther = 0;
                for (int i = 0; i < X.GetLength(1); i++)
                {
                    X1[i] = X[index1 - 1, i];
                    X2[i] = X[index2 - 1, i];
                }

                for (int i = 0; i < X.GetLength(0); i++)
                {
                    if ((i != index1 - 1) && (i != index2 - 1))
                    {
                        for (int l = 0; l < X.GetLength(1); l++)
                        {
                            XtempAll[l] = X[i, l];
                        }
                        switch (property)
                        {
                            case "min":
                                sumOther += regressionCoef[i + 1] * XtempAll.Min();
                                break;
                            case "avg":
                                sumOther += regressionCoef[i + 1] * XtempAll.Average();
                                break;
                            case "max":
                                sumOther += regressionCoef[i + 1] * XtempAll.Max();
                                break;
                        }
                    }
                }

                double localX1 = X1.Min();
                double localX2 = X2.Min();
                for (int i = 0; i < data.x1Data.Length; i++)
                {
                    data.x1Data[i] = localX1;
                    data.x2Data[i] = localX2;
                    localX1 += (X1.Max() - X1.Min()) / 50;
                    localX2 += (X2.Max() - X2.Min()) / 50;
                }
                for (int i = 0; i < 50; i++)
                {
                    for (int j = 0; j < 50; j++)
                    {
                        data.Y[i * data.x1Data.Length + j] = regressionCoef[0] * Math.Exp(regressionCoef[index1] * data.x1Data[j] + regressionCoef[index2] * data.x2Data[i] + sumOther);
                    }
                }

                return data;
            }

            public static ChartData3D Hyperbola3DChart(double[,] X, double[] regressionCoef, int index1, int index2, string property)
            {
                ChartData3D data = new ChartData3D(index1, index2, 2500, 50, 50);
                double[] X1 = new double[X.GetLength(1)];
                double[] X2 = new double[X.GetLength(1)];
                double[] XtempAll = new double[X.GetLength(1)];
                double sumOther = 0;
                for (int i = 0; i < X.GetLength(1); i++)
                {
                    X1[i] = X[index1 - 1, i];
                    X2[i] = X[index2 - 1, i];
                }

                for (int i = 0; i < X.GetLength(0); i++)
                {
                    if ((i != index1 - 1) && (i != index2 - 1))
                    {
                        for (int l = 0; l < X.GetLength(1); l++)
                        {
                            XtempAll[l] = X[i, l];
                        }
                        switch (property)
                        {
                            case "min":
                                sumOther += regressionCoef[i + 1] / XtempAll.Min();
                                break;
                            case "avg":
                                sumOther += regressionCoef[i + 1] / XtempAll.Average();
                                break;
                            case "max":
                                sumOther += regressionCoef[i + 1] / XtempAll.Max();
                                break;
                        }
                    }
                }

                double localX1 = X1.Min();
                double localX2 = X2.Min();
                for (int i = 0; i < data.x1Data.Length; i++)
                {
                    data.x1Data[i] = localX1;
                    data.x2Data[i] = localX2;
                    localX1 += (X1.Max() - X1.Min()) / 50;
                    localX2 += (X2.Max() - X2.Min()) / 50;
                }
                for (int i = 0; i < 50; i++)
                {
                    for (int j = 0; j < 50; j++)
                    {
                        data.Y[i * data.x1Data.Length + j] = regressionCoef[0] + regressionCoef[index1] / data.x1Data[j] + regressionCoef[index2] / data.x2Data[i] + sumOther;
                    }
                }

                return data;
            }

            public static ChartData3D Square3DChart(double[,] X, double[] regressionCoef, int index1, int index2, string property)
            {
                ChartData3D data = new ChartData3D(index1, index2, 2500, 50, 50);
                double[] X1 = new double[X.GetLength(1)];
                double[] X2 = new double[X.GetLength(1)];
                double[] XtempAll = new double[X.GetLength(1)];
                double sumOther = 0;
                for (int i = 0; i < X.GetLength(1); i++)
                {
                    X1[i] = X[index1 - 1, i];
                    X2[i] = X[index2 - 1, i];
                }

                for (int i = 0; i < X.GetLength(0); i++)
                {
                    if ((i != index1 - 1) && (i != index2 - 1))
                    {
                        for (int l = 0; l < X.GetLength(1); l++)
                        {
                            XtempAll[l] = X[i, l];
                        }
                        switch (property)
                        {
                            case "min":
                                sumOther += regressionCoef[i + 1] * Math.Sqrt(XtempAll.Min());
                                break;
                            case "avg":
                                sumOther += regressionCoef[i + 1] * Math.Sqrt(XtempAll.Average());
                                break;
                            case "max":
                                sumOther += regressionCoef[i + 1] * Math.Sqrt(XtempAll.Max());
                                break;
                        }
                    }
                }

                double localX1 = X1.Min();
                double localX2 = X2.Min();
                for (int i = 0; i < data.x1Data.Length; i++)
                {
                    data.x1Data[i] = localX1;
                    data.x2Data[i] = localX2;
                    localX1 += (X1.Max() - X1.Min()) / 50;
                    localX2 += (X2.Max() - X2.Min()) / 50;
                }
                for (int i = 0; i < 50; i++)
                {
                    for (int j = 0; j < 50; j++)
                    {
                        data.Y[i * data.x1Data.Length + j] = regressionCoef[0] + regressionCoef[index1] * Math.Sqrt(data.x1Data[j]) + regressionCoef[index2] * Math.Sqrt(data.x2Data[i]) + sumOther;
                    }
                }

                return data;
            }

            public static ChartData3D Degree3DChart(double[,] X, double[] regressionCoef, int index1, int index2, string property)
            {
                ChartData3D data = new ChartData3D(index1, index2, 2500, 50, 50);
                double[] X1 = new double[X.GetLength(1)];
                double[] X2 = new double[X.GetLength(1)];
                double[] XtempAll = new double[X.GetLength(1)];
                double sumOther = 1;
                for (int i = 0; i < X.GetLength(1); i++)
                {
                    X1[i] = X[index1 - 1, i];
                    X2[i] = X[index2 - 1, i];
                }

                for (int i = 0; i < X.GetLength(0); i++)
                {
                    if ((i != index1 - 1) && (i != index2 - 1))
                    {
                        for (int l = 0; l < X.GetLength(1); l++)
                        {
                            XtempAll[l] = X[i, l];
                        }
                        switch (property)
                        {
                            case "min":
                                sumOther *= Math.Pow(XtempAll.Min(), regressionCoef[i + 1]);
                                break;
                            case "avg":
                                sumOther *= Math.Pow(XtempAll.Average(), regressionCoef[i + 1]);
                                break;
                            case "max":
                                sumOther *= Math.Pow(XtempAll.Max(), regressionCoef[i + 1]);
                                break;
                        }
                    }
                }

                double localX1 = X1.Min();
                double localX2 = X2.Min();
                for (int i = 0; i < data.x1Data.Length; i++)
                {
                    data.x1Data[i] = localX1;
                    data.x2Data[i] = localX2;
                    localX1 += (X1.Max() - X1.Min()) / 50;
                    localX2 += (X2.Max() - X2.Min()) / 50;
                }
                for (int i = 0; i < 50; i++)
                {
                    for (int j = 0; j < 50; j++)
                    {
                        data.Y[i * data.x1Data.Length + j] = regressionCoef[0] * Math.Pow(data.x1Data[j], regressionCoef[index1]) * Math.Pow(data.x2Data[i], regressionCoef[index2]) * sumOther;
                    }
                }

                return data;
            }

            public static ChartData3D Polynomial3DChart(double[,] X, double[] regressionCoef, int index1, int index2, string property, int n)
            {
                ChartData3D data = new ChartData3D(index1, index2, 2500, 50, 50);
                double[] X1 = new double[X.GetLength(1)];
                double[] X2 = new double[X.GetLength(1)];
                double[] XtempAll = new double[X.GetLength(1)];
                double sumOther = 0;
                for (int i = 0; i < X.GetLength(1); i++)
                {
                    X1[i] = X[index1 - 1, i];
                    X2[i] = X[index2 - 1, i];
                }

                for (int i = 0; i < X.GetLength(0); i++)
                {
                    if ((i != index1 - 1) && (i != index2 - 1))
                    {
                        for (int h = 0; h < X.GetLength(1); h++)
                        {
                            XtempAll[h] = X[i, h];
                        }
                        int l = 1;
                        for (int s = i * n + 1; s < i * n + n + 1; s++)
                        {
                            switch (property)
                            {
                                case "min":
                                    sumOther += regressionCoef[s] * Math.Pow(XtempAll.Min(), l);
                                    break;
                                case "avg":
                                    sumOther += regressionCoef[s] * Math.Pow(XtempAll.Average(), l);
                                    break;
                                case "max":
                                    sumOther += regressionCoef[s] * Math.Pow(XtempAll.Max(), l);
                                    break;
                            }
                            l++;
                        }
                    }
                }

                double localX1 = X1.Min();
                double localX2 = X2.Min();
                for (int i = 0; i < data.x1Data.Length; i++)
                {
                    data.x1Data[i] = localX1;
                    data.x2Data[i] = localX2;
                    localX1 += (X1.Max() - X1.Min()) / 50;
                    localX2 += (X2.Max() - X2.Min()) / 50;
                }
                for (int i = 0; i < 50; i++)
                {
                    for (int j = 0; j < 50; j++)
                    {

                        double sum1 = 0;
                        double sum2 = 0;
                        int k1 = 1;
                        int k2 = 1;


                        for (int m = (index1 - 1) * n + 1; m < (index1 - 1) * n + n + 1; m++)
                        {
                            sum1 += regressionCoef[m] * Math.Pow(data.x1Data[j], k1);
                            k1++;
                        }


                        for (int m = (index2 - 1) * n + 1; m < (index2 - 1) * n + n + 1; m++)
                        {
                            sum2 += regressionCoef[m] * Math.Pow(data.x2Data[i], k2);
                            k2++;
                        }

                        data.Y[i * data.x1Data.Length + j] = regressionCoef[0] + sum1 + sum2 + sumOther;
                    }
                }

                return data;
            }
        }
    
}
